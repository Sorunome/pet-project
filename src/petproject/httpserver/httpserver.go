package httpserver

import (
	"log"
	"time"
	"net"
	"fmt"
	"net/http"
	"encoding/json"
	"petproject/config"
	"petproject/account"
	"petproject/auth"
	"petproject/util"
	"petproject/websocketserver"
	"petproject/pet"
	"petproject/petcollection"
	"html/template"
	"io/ioutil"
	"strings"
)

func getUniqueIdentAccount(w http.ResponseWriter, r *http.Request) *account.Account {
	ip := r.Header.Get(config.Get().Server.ForwardedFor)
	if ip == "" {
		var err error
		ip, _, err = net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			log.Print("Error parsing IP: ", err)
			return nil
		}
	}
	return auth.GetGuestAccount(ip)
}

var cookieRenewCache = map[string]int64{}
func getAccount(w http.ResponseWriter, r *http.Request) *account.Account {
	cookie, err := r.Cookie("auth")
	if err != nil {
		return getUniqueIdentAccount(w, r)
	}
	token := cookie.Value
	
	// check if the cookie needs to be refreshed, so if it is 10 days old
	curTime := time.Now().Unix()
	refresh := true
	if val, ok := cookieRenewCache[token]; ok {
		refresh = val + 60*60*24*10 < curTime
	}
	
	// shortcut if unknown account
	a := auth.GetAccountByToken(token, refresh)
	cookie.Path = "/"
	if a == nil {
		// remove token
		auth.DeleteToken(token)
		delete(cookieRenewCache, token)
		cookie.Value = ""
		cookie.Expires = time.Now().AddDate(0, -1, 0) // one month in past
		http.SetCookie(w, cookie)
		return getUniqueIdentAccount(w, r)
	}
	
	if refresh {
		cookie.Expires = time.Now().AddDate(0, 1, 0) // one month
		cookieRenewCache[token] = cookie.Expires.Unix()
		http.SetCookie(w, cookie)
	}
	return a
}

type templateGlobal struct {
	WebsocketBaseUrl string
	LoggedIn bool
	ValidAcc bool
	Account *account.TemplateAccount
	PetMethods []util.PetMethod
	PetMethodNames map[util.PetMethod]string
	Data interface{}
}
func getGlobalTemplateData(a *account.Account) *templateGlobal {
	t := new(templateGlobal)
	t.LoggedIn = a != nil && a.LoggedIn()
	t.ValidAcc = a != nil
	if t.ValidAcc {
		t.Account = a.GetTemplate()
	}
	t.PetMethods = util.AllPetMethods
	t.PetMethodNames = util.PetMethodNames
	t.WebsocketBaseUrl = config.Get().WebsocketBaseUrl
	return t
}

func GetPet(id int) *pet.TemplatePet {
	p := petcollection.Get(id)
	if p == nil {
		return nil
	}
	return p.GetTemplate()
}

func Add(a int, b int) int {
	return a + b
}

var funcMap = template.FuncMap{
	"getPet": GetPet,
	"add": Add,
}

var templates = map[string]*template.Template{}
func loadTemplatesDir(path string, allFiles []string) []string {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Print("Error scanning templates dir ", err)
	}
	for _, file := range files {
		filename := file.Name()
		if file.Mode().IsDir() {
			allFiles = loadTemplatesDir(path + "/" + filename, allFiles)
		} else if strings.HasSuffix(filename, ".html") {
			allFiles = append(allFiles, path + "/" + filename)
		}
	}
	return allFiles
}
func getTemplate(ident string) (*template.Template, error) {
	if t, ok := templates[ident]; ok {
		return t, nil
	}
	var allFiles []string
	allFiles = loadTemplatesDir("./html/templates/layout", allFiles)
	allFiles = append(allFiles, "./html/templates/" + ident + ".html")
	t, err := template.New("").Funcs(funcMap).ParseFiles(allFiles...)
	if err != nil {
		return nil, err
	}
	if !config.Get().Debug {
		templates[ident] = t
	}
	return t, nil
}

func internalServerError(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Header().Add("Content-Type", "text/plain")
	fmt.Fprintf(w, "%d - %s", http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError))
	if config.Get().Debug {
		fmt.Fprintf(w, "\n\n%v", err)
	}
}

func renderTemplate(a *account.Account, w http.ResponseWriter, path string, data interface{}) {
	t := getGlobalTemplateData(a)
	t.Data = data
	template, err := getTemplate(path)
	if err != nil {
		internalServerError(w, err)
		return
	}
	err = template.ExecuteTemplate(w, "base", t)
	if err != nil {
		internalServerError(w, err)
		return
	}
}

func handlePathInternal(path string, callback func(http.ResponseWriter, *http.Request)) {
	http.HandleFunc(path, callback)
	if path != "/" {
		http.HandleFunc(path + "/", callback)
	}
}

func handlePath(path string, callback func(http.ResponseWriter, *http.Request, *account.Account)) {
	handlePathInternal(path, func(w http.ResponseWriter, r *http.Request) {
		a := getAccount(w, r)
		callback(w, r, a)
	})
}

func handlePathLogin(path string, callback func(http.ResponseWriter, *http.Request, *account.Account)) {
	handlePathInternal(path, func(w http.ResponseWriter, r *http.Request) {
		a := getAccount(w, r)
		if a == nil || !a.LoggedIn() || !a.HavePermission("USER") {
			renderTemplate(a, w, "need_login", nil)
			return
		}
		callback(w, r, a)
	})
}

func handlePathFile(path string, file string) {
	handlePath(path, func(w http.ResponseWriter, r *http.Request, a *account.Account) {
		renderTemplate(a, w, file, nil)
	})
}

func replyJson(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	dataStr, err := json.Marshal(data)
	if err != nil {
		log.Print("Failed to json-ify response ", err)
		return
	}
	fmt.Fprintf(w, string(dataStr))
	return
}

func registerHandlers() {
	handlePathLogin("/settings/name", handleSettingsName)
	
	handlePathLogin("/settings/image", handleSettingsImage)
	
	handlePathLogin("/settings/image/delete", handleSettingsImageDelete)
	
	handlePathLogin("/settings/ban", handleSettingsBan)
	
	handlePathLogin("/settings", handleSettings)
	
	handlePath("/auth", handleAuth)
	
	handlePath("/login", handleLogin)
	
	handlePath("/logout", handleLogout)
	
	handlePath("/ws", websocketserver.Handle)
	
	handlePath("/about", handleAbout)
	
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./html/static"))))
	
	http.Handle("/uploads/", http.StripPrefix("/uploads/", http.FileServer(http.Dir(config.Get().Upload.Dir))))
	
	handlePath("/stats", handleStats)
	
	handlePath("/", handleRoot)
}

func Start() {
	log.Print("Starting server...")
	registerHandlers()
	websocketserver.RegisterHandlers()
	go websocketserver.Serve()
	err := http.ListenAndServe(config.Get().Server.Host, nil)
	if err != nil {
		log.Fatal("Failed to start http server: ", err)
	}
}
