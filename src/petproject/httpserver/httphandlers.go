package httpserver

import (
	"net/http"
	"net/url"
	"strings"
	"petproject/accountcollection"
	"petproject/petcollection"
	"petproject/pet"
	"petproject/account"
	"petproject/auth"
	"petproject/config"
	"petproject/util"
	"petproject/stats"
	"io/ioutil"
	"errors"
	"strconv"
	"fmt"
	"time"
	"log"
	"os"
	"crypto/sha1"
	"encoding/hex"
)

func getAffectingAccount(w http.ResponseWriter, r *http.Request, a *account.Account) (*account.Account, error) {
	petId, ok := r.URL.Query()["petId"]
	if !ok || len(petId[0]) < 1 {
		return a, nil
	}
	if !a.HavePermission("MODERATOR") {
		return nil, errors.New("Permission denied")
	}
	petIdInt, err := strconv.Atoi(petId[0])
	if err != nil {
		return nil, errors.New("Invalid pet ID")
	}
	changeAccount := accountcollection.GetByPet(petIdInt)
	if changeAccount == nil {
		return nil, errors.New("Pet not found")
	}
	return changeAccount, nil
}

func handleSettingsName(w http.ResponseWriter, r *http.Request, a *account.Account) {
	if r.Method == http.MethodPost {
		reply := struct {
			Success bool `json:"success"`
			Message string `json:"message"`
		}{false, ""}
		r.ParseForm()
		r.ParseMultipartForm(4096)
		name := r.Form["name"]

		changeAccount, err := getAffectingAccount(w, r, a)
		if err != nil {
			reply.Success = false
			reply.Message = err.Error()
			replyJson(w, reply)
			return
		}

		if len(name) < 1 || name[0] == "" {
			reply.Success = false
			reply.Message = "Blank name"
		} else {
			success, msg := changeAccount.SetName(name[0])
			if success {
				reply.Success = true
			} else {
				reply.Success = false
				reply.Message = msg
			}
		}

		// send reply
		replyJson(w, reply)
		return
	}
	renderTemplate(a, w, "settings/name", nil)
}

func handleSettingsImage(w http.ResponseWriter, r *http.Request, a *account.Account) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, http.StatusText(http.StatusMethodNotAllowed))
		return
	}

	reply := struct {
		Success bool `json:"success"`
		Message string `json:"message"`
		Image string `json:"image"`
	}{false, "", ""}

	file, handle, err := r.FormFile("image")
	if err != nil {
		reply.Message = err.Error()
		replyJson(w, reply)
		return
	}
	defer file.Close()
	if handle.Size > config.Get().Upload.MaxSize {
		reply.Message = "File too large"
		replyJson(w, reply)
		return
	}
	contentTypeHeader := handle.Header.Get("Content-Type")
	buffer := make([]byte, 512)
	_, err = file.Read(buffer)
	if err != nil {
		log.Print("Failed to detect content-type of file: ", err)
		replyJson(w, reply)
		return
	}
	contentType := http.DetectContentType(buffer)

	if contentType != contentTypeHeader {
		reply.Message = "Non-matching Content Type"
		replyJson(w, reply)
		return
	}

	validContentTypes := []string{"image/jpeg", "image/png", "image/jpg"}
	if !util.StringInArray(contentType, validContentTypes) {
		reply.Message = "Invalid file format!"
		replyJson(w, reply)
		return
	}

	fileEndings := map[string]string{
		"image/jpeg": ".jpg",
		"image/png": ".png",
		"image/jpg": ".jpg",
	}

	idUpper := strconv.FormatInt(int64(a.GetId() / 10000), 10)
	idLower := strconv.FormatInt(int64(a.GetId() % 10000), 10)

	basePath := config.Get().Upload.Dir + "/"

	if _, err := os.Stat(basePath + idUpper); os.IsNotExist(err) {
		// we need to create the dir
		err := os.MkdirAll(basePath + idUpper, os.ModePerm)
		if err != nil {
			log.Print("Failed to create directory: ", err)
		}
	}

	// delete the old image
	oldImage := a.GetPet().GetImage()
	if oldImage != "" {
		os.Remove(basePath + oldImage)
	}

	file.Seek(0, 0)
	data, err := ioutil.ReadAll(file)
	if err != nil {
		reply.Message = "Failed to read file"
		a.GetPet().SetImage("")
		replyJson(w, reply)
		return
	}
	hash := sha1.New()
	hash.Write(data)

	filePath := idUpper + "/" + idLower + "-" + hex.EncodeToString(hash.Sum(nil)) + fileEndings[contentType]
	err = ioutil.WriteFile(basePath + filePath, data, 0666)
	if err != nil {
		reply.Message = "Failed to save file"
		a.GetPet().SetImage("")
		replyJson(w, reply)
		return
	}
	reply.Success = true
	reply.Image = "/uploads/" + filePath
	a.GetPet().SetImage(filePath)
	replyJson(w, reply)
}

func handleSettingsImageDelete(w http.ResponseWriter, r *http.Request, a *account.Account) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, http.StatusText(http.StatusMethodNotAllowed))
		return
	}

	reply := struct {
		Success bool `json:"success"`
		Message string `json:"message"`
	}{false, ""}

	account, err := getAffectingAccount(w, r, a)
	if err != nil {
		reply.Success = false
		reply.Message = err.Error()
		replyJson(w, reply)
		return
	}

	// delete the old image
	basePath := config.Get().Upload.Dir + "/"
	oldImage := account.GetPet().GetImage()
	if oldImage != "" {
		os.Remove(basePath + oldImage)
	}
	account.GetPet().SetImage("")
	reply.Success = true
	replyJson(w, reply)
}

func handleSettingsBan(w http.ResponseWriter, r *http.Request, a *account.Account) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, http.StatusText(http.StatusMethodNotAllowed))
		return
	}

	reply := struct {
		Success bool `json:"success"`
		Message string `json:"message"`
	}{false, ""}

	account, err := getAffectingAccount(w, r, a)
	if err != nil {
		reply.Success = false
		reply.Message = err.Error()
		replyJson(w, reply)
		return
	}

	account.SetRole("BANNED")
	reply.Success = true
	replyJson(w, reply)
}

func handleSettings(w http.ResponseWriter, r *http.Request, a *account.Account) {
	renderTemplate(a, w, "settings/index", nil)
}

func handleAuth(w http.ResponseWriter, r *http.Request, a *account.Account) {
	urlParts := strings.Split(r.URL.Path, "/")
	if len(urlParts) < 3 {
		fmt.Fprintf(w, "No login method provided")
		return
	}
	if !auth.IsValidOauthType(urlParts[2]) {
		fmt.Fprintf(w, "invalid login method")
		return
	}

	params := r.URL.Query()
	state := params["state"]
	code := params["code"]
	if len(state) == 0 || len(code) == 0 {
		fmt.Fprintf(w, "missing state or code")
		return
	}
	success, msg, a := auth.HandleOauthLogin(urlParts[2], state[0], code[0], a)
	if !success {
		fmt.Fprintf(w, msg)
		return
	}
	if a == nil {
		fmt.Fprintf(w, "Account is nil....this is odd")
		return
	}
	cookieToken := auth.CreateCookieToken(a)
	cookie := http.Cookie{
		Name: "auth",
		Value: cookieToken,
		Path: "/",
		Expires: time.Now().AddDate(0, 1, 0),
	}
	http.SetCookie(w, &cookie)
	if a.GetName() == "" {
		http.Redirect(w, r, "/settings", 302)
		return
	}
	http.Redirect(w, r, "/", 302)
}

func handleLogin(w http.ResponseWriter, r *http.Request, a *account.Account) {
	urlParts := strings.Split(r.URL.Path, "/")
	if len(urlParts) < 3 || urlParts[2] == "" {
		renderTemplate(a, w, "login", nil)
		return
	}
	url := auth.GetOauthRedirectUrl(urlParts[2])
	if url == "" {
		fmt.Fprintf(w, "invalid login method")
		return
	}
	http.Redirect(w, r, url, 302)
}

func handleLogout(w http.ResponseWriter, r *http.Request, a *account.Account) {
	cookie, err := r.Cookie("auth")
	if err == nil {
		token := cookie.Value
		auth.DeleteToken(token)
		delete(cookieRenewCache, token)
		cookie.Value = ""
		cookie.Expires = time.Now().AddDate(0, -1, 0) // one month in past
		cookie.Path = "/"
		http.SetCookie(w, cookie)
	}
	http.Redirect(w, r, "/", 302)
}

type TemplateSocial struct {
	Name string
	Link string
}

type TemplateTeam struct {
	Pet *pet.TemplatePet
	Role string
	Social []*TemplateSocial
}

func handleStats(w http.ResponseWriter, r *http.Request, a *account.Account) {
	template := stats.GetTemplate()
	renderTemplate(a, w, "stats", template)
}

func handleAbout(w http.ResponseWriter, r *http.Request, a *account.Account) {
	team := make([]*TemplateTeam, 3, 3)
	nazzy := new(TemplateTeam)
	nazzy.Role = "Project lead"
	nazzy.Social = make([]*TemplateSocial, 0, 0)
//	nazzy.Social[0] = new(TemplateSocial)
//	nazzy.Social[0].Name = "twitter"
//	nazzy.Social[0].Link = "#"
	nazzy.Pet = petcollection.Get(2).GetTemplate()
	sugar := new(TemplateTeam)
	sugar.Role = "Front-end developer"
	sugar.Social = make([]*TemplateSocial, 1, 1)
	sugar.Social[0] = new(TemplateSocial)
	sugar.Social[0].Name = "twitter"
	sugar.Social[0].Link = "https://twitter.com/SugaryPoleCat"
//	sugar.Social[1] = new(TemplateSocial)
//	sugar.Social[1].Name = "discord"
//	sugar.Social[1].Link = "https://discordapp.com/channels/@me/289098255038676992"
	sugar.Pet = petcollection.Get(3).GetTemplate()
	soru := new(TemplateTeam)
	soru.Role = "Back-end developer"
	soru.Social = make([]*TemplateSocial, 3, 3)
	soru.Social[0] = new(TemplateSocial)
	soru.Social[0].Name = "matrix-org"
	soru.Social[0].Link = "https://matrix.to/#/@sorunome:sorunome.de"
	soru.Social[1] = new(TemplateSocial)
	soru.Social[1].Name = "github"
	soru.Social[1].Link = "https://github.com/Sorunome/"
	soru.Social[2] = new(TemplateSocial)
	soru.Social[2].Name = "gitlab"
	soru.Social[2].Link = "https://gitlab.com/Sorunome/"
	soru.Pet = petcollection.Get(1).GetTemplate()
	team[0] = nazzy
	team[1] = sugar
	team[2] = soru
	renderTemplate(a, w, "about", struct {
		Team []*TemplateTeam
	}{team})
}

func handleRoot(w http.ResponseWriter, r *http.Request, a *account.Account) {
	if len(r.URL.Path) > 1 {
		// we might have a pet
		urlPart := strings.ToLower(strings.Split(r.URL.Path, "/")[1])
		urlPart = url.QueryEscape(urlPart)
		p := petcollection.GetFromUrl(urlPart)
		if p != nil {
			petTemplate := p.GetTemplate()
			acc := accountcollection.GetByPet(petTemplate.Id)
			petTemplate.Account = acc.GetTemplate()
			if acc.HavePermission("USER") {
				renderTemplate(a, w, "pet", petTemplate)
			} else {
				renderTemplate(a, w, "pet_not_found", nil)
			}
		} else {
			renderTemplate(a, w, "pet_not_found", nil)
		}
	} else {
		pets := petcollection.GetRandomTemplates(5)
		renderTemplate(a, w, "index", struct {
			Pets []*pet.TemplatePet
		}{pets})
	}
}
