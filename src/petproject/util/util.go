package util

import (
	"math/rand"
	"time"
	"strconv"
)

type PetMethod string
const (
	PetPat = "pat"
	PetBoop = "boop"
	PetPet = "pet"
	PetHug = "hug"
	PetCuddle = "cuddle"
)

var AllPetMethods = []PetMethod {PetPat, PetBoop, PetPet, PetHug, PetCuddle};

var DefaultCooldowns = map[PetMethod]int64{
	PetPat: 5 * 1e9,
	PetBoop: 15 * 1e9,
	PetPet: 60 * 1e9,
	PetHug: 2*60 * 1e9,
	PetCuddle: 30*60 * 1e9,
}

var PetMethodXP = map[PetMethod]int64{
	PetPat: 0,
	PetBoop: 1,
	PetPet: 2,
	PetHug: 3,
	PetCuddle: 10,
}

var PetMethodNames = map[PetMethod]string{
	PetPat: "Pat",
	PetBoop: "Boop",
	PetPet: "Pet",
	PetHug: "Hug",
	PetCuddle: "Cuddle",
}

var InvalidPetNameUrls = []string {"help", "info", "about", "contact", "admin", "login", "logout", "action", "static", "html", "css", "js", "auth", "ws", "settings", "index", "stats"};

func StringInArray(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func IntInArray(a int, list []int) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func GetFromMap(m map[PetMethod]int64, k PetMethod, d int64) int64 {
	if val, ok := m[k]; ok {
		return val
	}
	return d
}

func IsValidPetMethod(method PetMethod) bool {
	for _, pm := range AllPetMethods {
		if pm == method {
			return true
		}
	}
	return false
}

const randCharBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
func RandomString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = randCharBytes[rand.Intn(len(randCharBytes))]
	}
	return string(b)
}

func RandomToken(ident string) string {
	str := RandomString(32)
	timestamp := strconv.FormatInt(time.Now().Unix(), 10)
	return ident + "|" + timestamp + "|" + str
}

func Min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func Contains(s []int, e int) bool {
	for _, a := range s {
		if (a == e) {
			return true
		}
	}
	return false
}
