package pet

import (
	"log"
	"database/sql"
	"petproject/db"
	"petproject/util"
	"encoding/json"
	"net/url"
	"strings"
	"strconv"
	"time"
)

type dbStatements struct {
	selectPet *sql.Stmt
	selectPetUrl *sql.Stmt
	updatePet *sql.Stmt
	insertPet *sql.Stmt
	doesPetUrlExist *sql.Stmt
}

var stmts *dbStatements = nil

type Pet struct {
	id int
	name string
	pets map[util.PetMethod]int64
	url string
	image string
	petsRecievedAt time.Time
	petsRecievedTotal int64
	havedb bool
	changed bool
}

type TemplatePet struct {
	Id int
	Name string
	Pets map[util.PetMethod]int64
	Url string
	Image string
	PetsRecievedAt time.Time
	PetsRecievedTotal int64
	Account interface{}
}

func openDb() *sql.DB {
	db := db.GetConnection()
	if stmts == nil {
		stmts = new(dbStatements)
		var err error
		stmts.selectPet, err = db.Prepare("SELECT id, name, pets, url, image, petsRecievedAt, petsRecievedTotal FROM pets WHERE id=$1")
		if err != nil {
			log.Print("Failed to prepare statement select pet ", err)
		}
		stmts.selectPetUrl, err = db.Prepare("SELECT id, name, pets, url, image, petsRecievedAt, petsRecievedTotal FROM pets WHERE url=$1")
		if err != nil {
			log.Print("Failed to prepare statement select pet ", err)
		}
		stmts.updatePet, err = db.Prepare("UPDATE pets SET name=$2, pets=$3, url=$4, image=$5, petsRecievedAt=$6, petsRecievedTotal=$7 WHERE id=$1")
		if err != nil {
			log.Print("Failed to prepare statement update pet ", err)
		}
		stmts.insertPet, err = db.Prepare("INSERT INTO pets (id, name, url) VALUES ($1, $2, $3)")
		if err != nil {
			log.Print("Failed to prepare statement insert pet ", err)
		}
		stmts.doesPetUrlExist, err = db.Prepare("SELECT 1 FROM pets WHERE url=$1")
		if err != nil {
			log.Print("Failed to prepare statement does pet url exist ", err)
		}
	}
	return db
}

func (p *Pet) Pet(method util.PetMethod) {
	if !util.IsValidPetMethod(method) {
		return
	}
	p.pets[method] = util.GetFromMap(p.pets, method, 0)
	p.pets[method]++
	p.petsRecievedAt = time.Now().UTC()
	p.petsRecievedTotal++
	p.changed = true
}

func (p *Pet) GetPets(method util.PetMethod) int64 {
	return util.GetFromMap(p.pets, method, 0)
}

func (p *Pet) GetUrl() string {
	return p.url
}

func (p *Pet) GetImage() string {
	return p.image
}

func (p *Pet) GetId() int {
	return p.id
}

func (p *Pet) GetTemplate() *TemplatePet {
	t := new(TemplatePet)
	t.Id = p.id
	t.Name = p.name
	t.Pets = p.pets
	t.Url = "/" + p.url
	if p.image != "" {
		t.Image = "/uploads/" + p.image
	}
	t.PetsRecievedAt = p.petsRecievedAt
	t.PetsRecievedTotal = p.petsRecievedTotal
	return t
}

func (t *TemplatePet) GetPets(method util.PetMethod) int64 {
	return util.GetFromMap(t.Pets, method, 0)
}

func (p *Pet) setUrl() {
	setUrl := strings.ToLower(p.name)
	setUrl = strings.Replace(setUrl, " ", "-", -1)
	setUrl = strings.Replace(setUrl, "/", "", -1)
	setUrl = url.QueryEscape(setUrl)
	if setUrl == p.url {
		// nothing to do
		return
	}
	var trash int
	
	err := stmts.doesPetUrlExist.QueryRow(setUrl).Scan(&trash)
	if util.StringInArray(setUrl, util.InvalidPetNameUrls) {
		err = nil
	}
	if err != nil {
		p.url = setUrl
		return
	}
	setUrl += "-"
	counter := int64(0)
	for err == nil {
		counter++
		err = stmts.doesPetUrlExist.QueryRow(setUrl + strconv.FormatInt(counter, 10)).Scan(&trash)
	}
	
	p.url = setUrl + strconv.FormatInt(counter, 10)
}

func (p *Pet) SetName(name string) {
	p.name = name
	p.setUrl()
	p.changed = true
}

func (p *Pet) SetImage(image string) {
	p.image = image
	p.changed = true
}

func (p *Pet) Save() {
	if !p.changed {
		return
	}
	openDb()
	var err error
	if !p.havedb {
		// ok create it first
		p.setUrl()
		_, err = stmts.insertPet.Exec(p.id, p.name, p.url)
		if err != nil {
			log.Print("Failed to insert new pet ", err)
		}
		p.havedb = true
	}
	jsonString, err := json.Marshal(p.pets)
	if err != nil {
		log.Print("Failed to json-ify pet data ", err)
		p.changed = false
		return
	}
	_, err = stmts.updatePet.Exec(p.id, p.name, string(jsonString), p.url, p.image, p.petsRecievedAt, p.petsRecievedTotal)
	if err != nil {
		log.Print("Failed to update pet ", err)
	}
	p.changed = false
}

func FromDB(id int) *Pet {
	openDb()
	res, err := stmts.selectPet.Query(id)
	log.Print("Getting pet by id ", id)
	if err != nil {
		defer res.Close()
		log.Print("Failed to get pet with id ", id, ": ", err)
		return nil
	}
	return FromDBData(res)
}

func FromDBUrl(url string) *Pet {
	openDb()
	log.Print("Getting pet by url ", url)
	res, err := stmts.selectPetUrl.Query(url)
	if err != nil {
		defer res.Close()
		log.Print("Failed to get pet with url ", url, ": ", err)
		return nil
	}
	return FromDBData(res)
}

func FromDBData(res *sql.Rows) *Pet {
	if !res.Next() {
		log.Print("Couldn't get non-existing pet")
		return nil
	}

	p := New(-1, "")
	p.changed = false
	p.havedb = true

	var jsonString []byte
	err := res.Scan(&p.id, &p.name, &jsonString, &p.url, &p.image, &p.petsRecievedAt, &p.petsRecievedTotal)
	if err != nil {
		log.Print("Failed to get pet: ", err)
		return nil
	}
	err = json.Unmarshal(jsonString, &p.pets)
	if err != nil {
		log.Print("Failed to de-json-ify pet data")
	}
	return p
}

func New(id int, name string) *Pet {
	p := new(Pet)
	p.id = id
	p.name = name
	p.url = ""
	p.pets = make(map[util.PetMethod]int64)
	p.petsRecievedAt = time.Unix(0, 0).UTC()
	p.petsRecievedTotal = 0
	p.havedb = false
	p.changed = true
	return p
}
