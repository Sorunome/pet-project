package auth

import (
	"log"
	"petproject/config"
	"petproject/util"
	"petproject/account"
	"petproject/accountcollection"
	"golang.org/x/oauth2"
	"strings"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"errors"
	"strconv"
	"context"
)

var oauthTypes = [...]string{
	"google",
	"discord",
	"vk",
	"github",
	"reddit",
	"gitlab",
}

var oauthConfigs = map[string]*oauth2.Config{}

var oauthEndpointsToSqlMap = map[string]account.LoginMethod{
	"guest": account.LoginGuest,
	"google": account.LoginGoogle,
	"facebook": account.LoginFacebook,
	"github": account.LoginGithub,
	"vk": account.LoginVk,
	"discord": account.LoginDiscord,
	"twitter": account.LoginTwitter,
	"reddit": account.LoginReddit,
	"tumblr": account.LoginTumblr,
	"gitlab": account.LoginGitlab,
}

var oauthEndpoints = map[string]oauth2.Endpoint{
	"google": oauth2.Endpoint{
		AuthURL: "https://accounts.google.com/o/oauth2/auth",
		TokenURL: "https://accounts.google.com/o/oauth2/token",
	},
	"discord": oauth2.Endpoint{
		AuthURL: "https://discordapp.com/api/oauth2/authorize",
		TokenURL: "https://discordapp.com/api/oauth2/token",
	},
	"vk": oauth2.Endpoint{
		AuthURL: "https://oauth.vk.com/authorize",
		TokenURL: "https://oauth.vk.com/access_token",
	},
	"github": oauth2.Endpoint{
		AuthURL: "https://github.com/login/oauth/authorize",
		TokenURL: "https://github.com/login/oauth/access_token",
	},
	"reddit": oauth2.Endpoint{
		AuthURL: "https://www.reddit.com/api/v1/authorize",
		TokenURL: "https://www.reddit.com/api/v1/access_token",
	},
	"gitlab": oauth2.Endpoint{
		AuthURL: "https://gitlab.com/oauth/authorize",
		TokenURL: "https://gitlab.com/oauth/token",
	},
}
var getIdentifier = map[string]func(*http.Client, *oauth2.Token)(string, error){
	"google": func(client *http.Client, token *oauth2.Token) (string, error) {
		resp, err := client.Get("https://www.googleapis.com/oauth2/v1/userinfo")
		if err != nil {
			return "", err
		}
		defer resp.Body.Close()
		data, _ := ioutil.ReadAll(resp.Body)
		jsonData := struct {
			Id string `json:"id"`
		}{""}
		err = json.Unmarshal(data, &jsonData)
		if err != nil {
			log.Print(err)
			return "", err
		}
		if jsonData.Id == "" {
			return "", errors.New("invalid ID")
		}
		return jsonData.Id, nil
	},
	"discord": func(client *http.Client, token *oauth2.Token) (string, error) {
		resp, err := client.Get("https://discordapp.com/api/users/@me")
		if err != nil {
			return "", err
		}
		defer resp.Body.Close()
		data, _ := ioutil.ReadAll(resp.Body)
		jsonData := struct {
			Id string `json:"id"`
		}{""}
		err = json.Unmarshal(data, &jsonData)
		if err != nil {
			log.Print(err)
			return "", err
		}
		if jsonData.Id == "" {
			return "", errors.New("invalid ID")
		}
		return jsonData.Id, nil
	},
	"vk": func(client *http.Client, token *oauth2.Token) (string, error) {
		resp, err := client.Get("https://api.vk.com/method/users.get?v=5.64&access_token=" + token.AccessToken)
		if err != nil {
			return "", err
		}
		defer resp.Body.Close()
		data, _ := ioutil.ReadAll(resp.Body)
		jsonData := struct{
			Response []struct{
				Id int64 `json:"id"`
			} `json:"response"`
		}{make([]struct{Id int64 `json:"id"`}, 0)}
		err = json.Unmarshal(data, &jsonData)
		if err != nil {
			log.Print(err)
			return "", err
		}
		if len(jsonData.Response) == 0 || jsonData.Response[0].Id == 0 {
			return "", errors.New("invalid ID")
		}
		return strconv.FormatInt(jsonData.Response[0].Id, 10), nil
	},
	"github": func(client *http.Client, token *oauth2.Token) (string, error) {
		resp, err := client.Get("https://api.github.com/user")
		if err != nil {
			return "", err
		}
		defer resp.Body.Close()
		data, _ := ioutil.ReadAll(resp.Body)
		jsonData := struct{
			Id int64 `json:"id"`
		}{0}
		err = json.Unmarshal(data, &jsonData)
		if err != nil {
			log.Print(err)
			return "", err
		}
		if jsonData.Id == 0 {
			return "", errors.New("invalid ID")
		}
		return strconv.FormatInt(jsonData.Id, 10), nil
	},
	"reddit": func(client *http.Client, token *oauth2.Token) (string, error) {
		resp, err := client.Get("https://oauth.reddit.com/api/v1/me")
		if err != nil {
			log.Print(err)
			return "", err
		}
		defer resp.Body.Close()
		data, _ := ioutil.ReadAll(resp.Body)
		jsonData := struct{
			Id string `json:"id"`
		}{""}
		err = json.Unmarshal(data, &jsonData)
		if err != nil {
			log.Print(err)
			return "", err
		}
		if jsonData.Id == "" {
			return "", errors.New("invalid ID")
		}
		return jsonData.Id, nil
	},
	"gitlab": func(client *http.Client, token *oauth2.Token) (string, error) {
		resp, err := client.Get("https://gitlab.com/api/v4/user")
		if err != nil {
			log.Print(err)
			return "", err
		}
		defer resp.Body.Close()
		data, _ := ioutil.ReadAll(resp.Body)
		jsonData := struct{
			Id int64 `json:"id"`
		}{0}
		err = json.Unmarshal(data, &jsonData)
		if err != nil {
			log.Print(err)
			return "", err
		}
		if jsonData.Id == 0 {
			return "", errors.New("invalid ID")
		}
		return strconv.FormatInt(jsonData.Id, 10), nil
	},
}

var oauthScopes = map[string][]string{
	"google": {"profile"},
	"discord": {"identify"},
	"vk": {"status"},
	"github": {"read:user"},
	"reddit": {"identity"},
	"gitlab": {"read_user"},
}

func getConfigForType(authType string) *config.AuthConfigKeypair {
	switch authType {
	case "google":
		return config.Get().Auth.Google
	case "discord":
		return config.Get().Auth.Discord
	case "vk":
		return config.Get().Auth.Vk
	case "github":
		return config.Get().Auth.Github
	case "reddit":
		return config.Get().Auth.Reddit
	case "gitlab":
		return config.Get().Auth.Gitlab
	}
	return nil
}

func IsValidOauthType(authType string) bool {
	return getConfigForType(authType) != nil
}

func getConfig(authType string) *oauth2.Config {
	if !IsValidOauthType(authType) {
		return nil
	}
	if conf, ok := oauthConfigs[authType]; ok {
		return conf
	}
	yamlConf := getConfigForType(authType)
	c := &oauth2.Config {
		ClientID: yamlConf.Id,
		ClientSecret: yamlConf.Secret,
		RedirectURL: config.Get().BaseUrl + "/auth/" + authType,
		Scopes: oauthScopes[authType],
		Endpoint: oauthEndpoints[authType],
	}
	oauthConfigs[authType] = c
	return oauthConfigs[authType]
}

var validTokens []string
func generateToken(authType string) string {
	token := util.RandomToken(authType)
	validTokens = append(validTokens, token)
	return token
}

type oauthRoundTripper struct {
	Proxied http.RoundTripper
}

func (rt oauthRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header.Set("User-Agent", "Pet Project")
	return rt.Proxied.RoundTrip(req)
}

func getOauthContext() context.Context {
	client := &http.Client{
		Transport: oauthRoundTripper{http.DefaultTransport},
	}
	return context.WithValue(context.TODO(), oauth2.HTTPClient, client)
}

func GetOauthRedirectUrl(authType string) string {
	conf := getConfig(authType)
	if conf == nil {
		return ""
	}
	s := conf.AuthCodeURL(generateToken(authType))
	return s
}

func HandleOauthLogin(authType string, state string, code string, a *account.Account) (bool, string, *account.Account) {
	conf := getConfig(authType)
	if conf == nil {
		return false, "invalid login method", nil
	}
	if strings.Split(state, "|")[0] != authType {
		return false, "unmatched auth type", nil
	}
	i := -1
	for k, haveState := range validTokens {
		if haveState == state {
			i = k
			break
		}
	}
	if i == -1 {
		return false, "Invalid state", nil
	}
	// remove that token, those are one-time use
	validTokens = append(validTokens[:i], validTokens[i+1:]...)
	
	token, err := conf.Exchange(getOauthContext(), code)
	if err != nil {
		log.Print("Failed fetching token: ", err)
		return false, "Failed to fetch token", nil
	}
	client := conf.Client(getOauthContext(), token)
	id, err := getIdentifier[authType](client, token)
	if err != nil || id == "" {
		log.Print("Failed to get identifier: ", err)
		return false, "Failed to fetch identifier", nil
	}
	// okay, we are logged in and have the ID!
	a2 := accountcollection.GetAuth(oauthEndpointsToSqlMap[authType], id)
	if a2 != nil {
		// we are done!
		return true, "", a2
	}
	// we will have to create a new account....
	if a != nil && !a.LoggedIn() {
		// we have a guest account to update
		a.SetAuth(oauthEndpointsToSqlMap[authType], id)
	} else {
		a = accountcollection.GetOrCreateAuth(oauthEndpointsToSqlMap[authType], id)
	}
	return true, "", a
}
