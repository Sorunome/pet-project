package config

import (
	"log"
	"os"
	"io/ioutil"
	"gopkg.in/yaml.v2"
)

type DatabaseConfig struct {
	Postgres string `yaml:"postgres"`
	FlushInterval int `yaml:"flushInterval"`
	MaintenanceInterval int `yaml:"maintenanceInterval"`
	FlushCacheInterval int `yaml:"flushCacheInterval"`
}

type ServerConfig struct {
	Host string `yaml:"host"`
	ForwardedFor string `yaml:"forwardedFor"`
}

type AuthConfigKeypair struct {
	Id string `yaml:"id"`
	Secret string `yaml:"secret"`
}

type AuthConfig struct {
	Google *AuthConfigKeypair `yaml:"google"`
	Discord *AuthConfigKeypair `yaml:"discord"`
	Vk *AuthConfigKeypair `yaml:"vk"`
	Github *AuthConfigKeypair `yaml:"github"`
	Reddit *AuthConfigKeypair `yaml:"reddit"`
	Gitlab *AuthConfigKeypair `yaml:"gitlab"`
}

type UploadConfig struct {
	Dir string `yaml:"dir"`
	MaxSize int64 `yaml:"maxSize"`
}

type Config struct {
	WebsocketBaseUrl string `yaml:"websocketBaseUrl"`
	BaseUrl string `yaml:"baseUrl"`
	Debug bool `yaml:"debug"`
	Database *DatabaseConfig `yaml:"database"`
	Server *ServerConfig `yaml:"server"`
	Auth *AuthConfig `yaml:"auth"`
	Upload *UploadConfig `yaml:"upload"`
}

var instance *Config = nil
var Path = "config.yaml"

func defaultConfig() *Config {
	return &Config{
		WebsocketBaseUrl: "ws://localhost:8080",
		BaseUrl: "http://localhost:8080",
		Debug: false,
		Database: &DatabaseConfig{
			Postgres: "postgres://username:password@host/database?sslmode=disable",
			FlushInterval: 20,
			MaintenanceInterval: 3600,
			FlushCacheInterval: 3600*24,
		},
		Server: &ServerConfig{
			Host: "localhost:8080",
			ForwardedFor: "",
		},
		Auth: &AuthConfig{},
		Upload: &UploadConfig{
			Dir: "./uploads",
			MaxSize: 1024*1024*4,
		},
	}
}

func loadConfig() error {
	c := defaultConfig()

	f, err := os.Open(Path)
	if err != nil {
		return err
	}
	defer f.Close()

	buffer, err := ioutil.ReadAll(f)
	if err != nil {
		return err
	}
	err = yaml.Unmarshal(buffer, &c)
	if err != nil {
		return err
	}
	instance = c
	return nil
}

func Get() *Config {
	if instance != nil {
		return instance
	}
	err := loadConfig()
	if err != nil {
		log.Panic("Error loading config: ", err)
	}
	return instance
}
