package websocketserver

import (
	"log"
	"petproject/account"
	"encoding/json"
	"petproject/petcollection"
	"petproject/util"
	"time"
)

func handlePet(a *account.Account, msg string, c *Client) {
	if !a.HavePermission("USER") {
		c.SendError("Banned")
		return
	}
	msgJson := struct {
		Id int `json:"id"`
		Method util.PetMethod `json:"method"`
	}{-1, ""}
	err := json.Unmarshal([]byte(msg), &msgJson)
	if err != nil {
		c.SendError("Invalid JSON")
		return
	}
	method := msgJson.Method
	id := msgJson.Id
	p := petcollection.Get(id)
	if p == nil {
		c.SendError("Unknown Pet")
		return
	}
	if !a.Appease(method, p) {
		c.SendError("Couldn't pet")
		return
	}
	replyJson := struct {
		Type string `json:"type"`
		Method string `json:"method"`
		Id int `json:"id"`
		Count int64 `json:"count"`
	}{"pets", string(method), id, p.GetPets(method)}
	replyStr, err := json.Marshal(replyJson)
	if err != nil {
		log.Print("Failed to json-ify reply ", err)
		return
	}
	// broadcast new count
	c.hub.broadcastPet <- &petMessage{
		id: id,
		msg: string(replyStr),
	}
	// send new cooldown
	cooldownJson := struct {
		Type string `json:"type"`
		Methods map[util.PetMethod]int64 `json:"methods"`
	}{"cooldown", make(map[util.PetMethod]int64)}
	curTime := time.Now().UnixNano()
	cd := int64(0)
	appeaseTime := a.GetCooldown(method)
	if appeaseTime > curTime {
		cd = (appeaseTime - curTime) / 1e6
	}
	cooldownJson.Methods[method] = cd
	cooldownString, err := json.Marshal(cooldownJson)
	if err != nil {
		log.Print("Failed to json-ify reply ", err)
		return
	}
	c.send <- string(cooldownString)
}

func handleSubPet(a *account.Account, msg string, c *Client) {
	msgJson := struct {
		Id int `json:"id"`
	}{-1}
	err := json.Unmarshal([]byte(msg), &msgJson)
	if err != nil {
		c.SendError("Invalid JSON")
		return
	}
	c.petSubs = append(c.petSubs, msgJson.Id)
}

func handleCooldowns(a *account.Account, msg string, c *Client) {
	replyJson := struct {
		Type string `json:"type"`
		Methods map[util.PetMethod]int64 `json:"methods"`
	}{"cooldown", make(map[util.PetMethod]int64)}
	curTime := time.Now().UnixNano()
	for _, method := range util.AllPetMethods {
		cd := int64(0)
		appeaseTime := a.GetCooldown(method)
		if appeaseTime > curTime {
			cd = (appeaseTime - curTime) / 1e6
		}
		replyJson.Methods[method] = cd
	}
	replyStr, err := json.Marshal(replyJson)
	if err != nil {
		log.Print("Failed to json-ify reply ", err)
		return
	}
	c.send <- string(replyStr)
}
