package websocketserver

import (
	"log"
	"net/http"
	"time"
	"petproject/account"
	"github.com/gorilla/websocket"
	"strings"
	"encoding/json"
)

const (
	writeWait = 10 * time.Second
	pongWait = 60 * time.Second
	pingPeriod = (pongWait * 9) / 10
	maxMessageSize = 512
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		// TODO: actually check stuff
		return true
	},
}

var handlers = map[string]func(*account.Account, string, *Client){}

type Client struct {
	hub *Hub
	conn *websocket.Conn
	account *account.Account
	send chan string
	petSubs []int
}

func (c *Client) SendError(msg string) {
	reply := struct {
		Error string `json:"error"`
	}{msg}
	replyStr, err := json.Marshal(reply)
	if err == nil {
		c.send <- string(replyStr)
	}
}

func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error {
		c.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Print("Websocket receiving error: ", err)
			}
			break
		}
		messageStr := string(message)
		messageParts := strings.Split(messageStr, "\n")
		for _, msg := range messageParts {
			if msg != "" {
				msgJson := struct {
					Type string `json:"type"`
				}{""}
				err = json.Unmarshal([]byte(msg), &msgJson)
				if err != nil {
					log.Print("Error decoding websocket frame: ", err)
					c.SendError("Invalid JSON")
				} else {
					if handler, ok := handlers[msgJson.Type]; ok {
						handler(c.account, msg, c)
					}
				}
			}
		}
	}
}

func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// we want to quit this connection
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write([]byte(message))
			
			// flush queue
			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write([]byte("\n" + <-c.send))
			}
			
			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

func handleType(frameType string, callback func(*account.Account, string, *Client)) {
	handlers[frameType] = callback
}

func handleTypeAccount(frameType string, callback func(*account.Account, string, *Client)) {
	handleType(frameType, func(a *account.Account, msg string, c *Client) {
		if a == nil {
			return
		}
		callback(a, msg, c)
	})
}

func RegisterHandlers() {
	handleTypeAccount("pet", handlePet)
	handleType("sub_pet", handleSubPet)
	handleTypeAccount("cooldowns", handleCooldowns)
}

var hub *Hub = nil
func Handle(w http.ResponseWriter, r *http.Request, a *account.Account) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("Failed to upgrade websocket connection: ", err)
		return
	}
	client := &Client{
		hub: hub,
		conn: conn,
		account: a,
		send: make(chan string, 256),
		petSubs: make([]int, 0),
	}
	hub.register <- client
	go client.writePump()
	go client.readPump()
}

func Serve() {
	hub = newHub()
	hub.run()
}
