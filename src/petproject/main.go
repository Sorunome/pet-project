package main

import (
	"math/rand"
	"time"
	"log"
	"os"
	"os/signal"
	"syscall"
	"petproject/db"
	"petproject/httpserver"
	"petproject/accountcollection"
	"petproject/petcollection"
	"petproject/config"
	"petproject/auth"
	"petproject/stats"
)

func saveDbStuff() {
	accountcollection.Save()
	petcollection.Save()
	stats.Action(stats.ActionSave)
}

func startPollingSaves() {
	for {
		time.Sleep(time.Duration(config.Get().Database.FlushInterval) * time.Second)
		go saveDbStuff()
	}
}

func maintenanceDbStuff() {
	accountcollection.Maintenance()
	auth.Maintenance()
}

func startPollingMaintenance() {
	for {
		time.Sleep(time.Duration(config.Get().Database.MaintenanceInterval) * time.Second)
		go maintenanceDbStuff()
	}
}

func cacheDbStuff() {
	stats.Action(stats.ActionFlushCache)
}

func startPollingCache() {
	for {
		time.Sleep(time.Duration(config.Get().Database.FlushCacheInterval) * time.Second)
		go cacheDbStuff()
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())
	db.Startup()
	go startPollingSaves()
	go startPollingMaintenance()
	go startPollingCache()
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		log.Print("Got SIGTERM, quitting...")
		saveDbStuff()
		maintenanceDbStuff() // we don't want the interval so seldomly this never gets called....
		// and no syncronously quit the stats stuff
		stats.FlushAll()
		os.Exit(1)
	}()
	stats.Load()
	// load up a few pets
	dbConn := db.GetConnection()
	res, err := dbConn.Query("SELECT id FROM pets ORDER BY petsrecievedat DESC LIMIT 100")
	if err != nil {
		log.Panic("Failed to load existing pets", err)
	}
	for res.Next() {
		id := 0
		err = res.Scan(&id)
		if (err != nil) {
			log.Print("Failed to load pet id", err)
		}
		petcollection.Get(id)
	}
	httpserver.Start()
}
