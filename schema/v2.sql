-- add role column

ALTER TABLE accounts ADD role VARCHAR(255) NOT NULL DEFAULT 'USER';
