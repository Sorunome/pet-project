-- create and populate schema table
CREATE TABLE schema (
	version INTEGER UNIQUE NOT NULL
);
INSERT INTO schema VALUES (0);

-- create pets table
CREATE TABLE pets (
	id INT NOT NULL PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	pets TEXT NOT NULL DEFAULT '',
	url VARCHAR(255) NOT NULL,
	image VARCHAR(255) NOT NULL DEFAULT '',
	petsRecievedAt TIMESTAMP,
	petsRecievedTotal BIGINT NOT NULL DEFAULT 0,
	created_at TIMESTAMP NOT NULL DEFAULT NOW(),
	
	UNIQUE(url)
);

-- create accounts table
CREATE TABLE accounts (
	id SERIAL PRIMARY KEY,
	auth VARCHAR(255) NOT NULL DEFAULT '',
	loginMethod INT NOT NULL DEFAULT 0,
	xp BIGINT NOT NULL DEFAULT 0,
	name VARCHAR(255) NOT NULL DEFAULT '',
	cooldown TEXT NOT NULL DEFAULT '',
	nextAppease TEXT NOT NULL DEFAULT '',
	petsGiven TEXT NOT NULL DEFAULT '',
	petsGivenAt TIMESTAMP,
	petsGivenTotal BIGINT NOT NULL DEFAULT 0,
	created_at TIMESTAMP NOT NULL DEFAULT NOW(),
	
	UNIQUE(auth, loginMethod)
);

-- create tokens table
CREATE TABLE tokens (
	id SERIAL PRIMARY KEY,
	user_id INT NOT NULL,
	token VARCHAR(255) NOT NULL,
	timeout TIMESTAMP NOT NULL DEFAULT NOW() + INTERVAL '1 month',
	
	UNIQUE(user_id, token)
);

-- create stats_most_per_time table
CREATE TABLE stats_most_per_time (
	id SERIAL PRIMARY KEY,
	time_ident VARCHAR(16) NOT NULL,
	other_id INT NOT NULL,
	amount BIGINT NOT NULL,
	given_type SMALLINT NOT NULL,
	time_span SMALLINT NOT NULL,
	place INT NOT NULL,
	
	UNIQUE(time_ident, given_type, time_span, other_id),
	UNIQUE(time_ident, given_type, time_span, other_id, place)
);

-- create stats_pets_per_time table
CREATE TABLE stats_pets_per_time (
	id SERIAL PRIMARY KEY,
	time_ident VARCHAR(16) NOT NULL,
	amount BIGINT NOT NULL,
	
	UNIQUE(time_ident)
);

-- create stats_cache_pets table
CREATE TABLE stats_cache_pets (
	id SERIAL PRIMARY KEY,
	other_id INT NOT NULL,
	time_span SMALLINT NOT NULL,
	given_type SMALLINT NOT NULL,
	amount BIGINT NOT NULL,
	
	UNIQUE(other_id, time_span, given_type)
);
