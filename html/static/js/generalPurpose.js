window.addEventListener('load', (evt)=>{
    let content;
    if(page){
        switch(page){
            case 'upper':
                content = document.getElementById('general-bottom-container');
                break;
            case 'lower':
                content = document.getElementById('general-top-container');
                break;
            default:
                break;
        }
        if(content) content.style.display = 'none';
    }
    if(container){
        const containerController = document.getElementById('container-controller');
        switch(container){
            case 'pet':
                containerController.classList.remove('container-md');
                containerController.classList.add('container-fluid');
                break;
            default:
                break;
        }
    }
    const randomTxt = document.getElementById("index-random-lower");
    if(randomTxt){
        const randomTexts = [
            "hyperactive!",
            "annoying!",
            "not safe for normies!",
            "working from home!",
            "impossible!",
            "lovely!",
            "explosive!",
            "petting!",
            "town!",
            "^~^",
            "you have clicked 100000 times!",
            "taking over the internet!",
            "shiny!",
            "bonjour!",
            "hi again!",
            "overflowing!",
            "come back soon!",
            "happy to see you back!",
            "have you finished your homework?",
            "party!",
            "sweet!",
            "100% sugar!",
            "120% sugar!",
            "meme",
            "cute~",
            "casual",
            "users online: 9001",
            "don't feed birds chocolate!",
            "wash your hands!",
            "don't touch your face!",
            "12761 clicks per second!",
            "fat free!",
            "unfair!",
            "capitalism simulator!",
            "hugs for everyone!",
            "gnu petting!",
            "google-free!",
            "now supports gitlab!",
            "unlimited power!",
            "meow!",
            "groundbreaking!",
            "addictive!",
            "less addictive than cookie clicker!",
            "less addictive than facebook!",
            "less addictive than discord!",
            "tl;dr",
            "something funny!",
            "sqrt(-1) love you!",
            "fun!",
            "yay!",
            "endless!",
            "toilet friendly!",
            "[petting intensifies]",
            "harmless!",
            "remember to stretch!",
            "^.^",
            "^_^",
            "OwO",
            "Owo",
            "UwU"
        ];
        randomTxt.innerText = randomTexts[Math.floor(Math.random() * randomTexts.length)];
    }
});