const handlePackets = {};
let ws;

function connect(isStartup){
	const wsAlert = document.getElementById('websocket-alert');
	ws = new WebSocket(webSocketBaseUrl + "/ws");
	// init variables
	// init websocket stuffs
	ws.onclose = (e) => {
		wsAlert.innerText = 'Disconnected from server!';
		wsAlert.classList.add('errormsg-error');
		wsAlert.classList.add('websocket-alert-show');
		setTimeout(function(){
			connect(false);
		}, 1000);
	};
	ws.onmessage = (e) => {
		try {
			e.data.split("\n").map((d) => {
				const data = JSON.parse(d);
				if (handlePackets[data.type]) {
					handlePackets[data.type](data);
				}
			});
		} catch (e) {
			console.error(e);
		}
	};
	ws.onerror = (e) => {
		console.error(e);
	};
	ws.onopen = (e) => {
		if(!isStartup){
			wsAlert.classList.remove('errormsg-error');
			wsAlert.classList.add('errormsg-success');
			wsAlert.innerText = 'Reconnected to server!';
			setTimeout(function(){
				wsAlert.innerText = '';
				wsAlert.classList.remove('errormsg-success');
			}, 3000);
		}
		ws.send(JSON.stringify({
			type: "sub_pet",
			id: petId,
		}));
		ws.send(JSON.stringify({
			type: "cooldowns",
		}));
	}
}
window.addEventListener("load", async () => {
	const wsAlert = document.getElementById('websocket-alert');
	// set that we started the webpage to disable the constnat reload/load of the dialog window
	wsAlert.classList.remove('errormsg-success');
	wsAlert.classList.remove('errormsg-error');
	if (maxCooldowns) {
		// else we can't pet anyways
		const curCooldowns = {};
		const btnOverlay = {};
		const btnOverlayTxt = {};
		const statRecieved = {};
		const btnText = {};
		const btn = {};
		for (const m of petMethods) {
			curCooldowns[m] = 0;
			btn[m] = document.getElementById('pet-' + petId + '-' + m + '-button');
			btnOverlay[m] = document.getElementById('pet-' + petId + '-' + m + '-overlay');
			btnOverlayTxt[m] = document.getElementById('pet-' + petId + '-' + m + '-overlay-text');
			statRecieved[m] = document.getElementById('pet-' + petId + '-' + m + '-received');
			btnText[m] = document.getElementById('pet-' + petId + '-' + m + '-button-text');
		}

		// register pet button clicking
		for (const button of document.getElementsByClassName("pet-data-action")) {
			button.addEventListener("click", function(e) {
				e.preventDefault();
				const method = this.dataset.method;
				const selfError = document.getElementById('pet-self-error');
				if(petId == ownPetId){
					selfError.innerHTML = 'You can not give affection to yourself!';
					selfError.classList.add('errormsg-error');
					for(const m of petMethods){
						btn[m].classList.add('errormsg-error');
					}
				}
				else{
					selfError.classList.remove('errormsg-error');
					if (curCooldowns[method] <= 0) {
						ws.send(JSON.stringify({
							type: "pet",
							method: method,
							id: petId,
						}));
					}
					const el = document.createElement('div');
					el.classList.add('pet-button-plus-one');
					el.innerHTML = '+1';
					button.appendChild(el);
					el.addEventListener('animationend', (aaa)=>{
						el.style.display = 'none';
					});
				}
			});
		}

		// register websocket callbacks
		handlePackets["pets"] = (data) => {
			if (data.id == petId) {
				document.getElementById("pet-" + data.id + "-" + data.method + "-received").innerText = data.count;
			}
		}
		handlePackets["cooldown"] = (data) => {
			for (const method in data.methods) {
				if (data.methods.hasOwnProperty(method)) {
					curCooldowns[method] = data.methods[method];
				}
			}
		}

		// update cooldown stuff
		let lastTime;
		const renderTimeout = (thisTime) => {
			if (!lastTime) {
				lastTime = thisTime;
			}
			const dt = thisTime - lastTime;
			lastTime = thisTime;
			for (const m of petMethods) {
				if (curCooldowns[m] >= 0) {
					curCooldowns[m] -= dt;
					const time = curCooldowns[m];
					if (time > 0) {
						const percent = time / maxCooldowns[m];
						const countdownText = Math.floor(time / 1000);
						btnText[m].style.opacity = 0;
						btnOverlay[m].style.display = 'block';
						btnOverlay[m].style.opacity = percent;
						btnOverlayTxt[m].style.display = 'block';
						btnOverlayTxt[m].style.opacity = 1;
						let minutes = Math.floor(countdownText / 60);
						let seconds = countdownText % 60;
						let timeDisp = '';
						if(minutes > 0){
							timeDisp += '' + minutes + 'm : ' + (seconds < 10 ? '0' : '');
						}
						timeDisp += '' + seconds + 's';
						btnOverlayTxt[m].innerText = timeDisp.toString();
					} else {
						// Reset everything after timer reaches 0;
						btnText[m].style.opacity = 1;
						btnOverlayTxt[m].style.opacity = 0;
						btnOverlayTxt[m].style.display = 'none';
						btnOverlay[m].style.opacity = 0;
						btnOverlay[m].style.display = 'none';
					}
				}
			}
			window.requestAnimationFrame(renderTimeout);
		};

		window.requestAnimationFrame(renderTimeout);
	}

	const petImage = document.getElementById('pet-image');
	const petImageSizer = document.getElementById('pet-image-size')
	if(petImageSizer){
		petImageSizer.addEventListener('click', (evt)=>{
			petImage.classList.toggle('pet-image-limiter');
		});
	}

	const petLinkBtn = document.getElementById('pet-link');
	if(petLinkBtn){
		petLinkBtn.addEventListener('click', function(){
			copyToClip(petLink);
			let alertMsg = document.getElementById('pet-link-copied');
			alertMsg.innerHTML = "Link copied to clipboard!";
			alertMsg.classList.add('errormsg-success');
			let timeOut = setTimeout(function(){
				alertMsg.classList.remove('errormsg-success');
				alertMsg.innerHTML = "";
				window.clearTimeout(timeOut);
			}, 5000);
		});

		const copyToClip = str => {
			const el = document.createElement('textarea');
			el.value = str;
			el.setAttribute('readonly', '');
			el.style.position = 'absolute';
			el.style.left = '-99999px';
			document.body.appendChild(el);
			el.select();
			document.execCommand('copy');
			document.body.removeChild(el);
		}
	}
	connect(true);
});
// document.getElementById('pet-get-link').addEventListener('click', copy());
